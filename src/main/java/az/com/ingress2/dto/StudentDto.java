package az.com.ingress2.dto;

import lombok.Data;

@Data
public class StudentDto {
    private int id;
    private String name;
    private String surname;
    private String birtdate;
}
