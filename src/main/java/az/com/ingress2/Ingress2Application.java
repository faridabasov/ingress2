package az.com.ingress2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class Ingress2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ingress2Application.class, args);
	}

}
