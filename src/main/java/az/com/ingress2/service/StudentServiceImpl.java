package az.com.ingress2.service;

import az.com.ingress2.dto.StudentDto;
import az.com.ingress2.dto.StudentInDto;
import az.com.ingress2.entity.Student;
import az.com.ingress2.repository.StrudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StrudentRepository repo;
    private final ModelMapper mapper;


    @Override
    public StudentDto createStudent(StudentInDto dto) {
        Student std = repo.save(mapper.map(dto, Student.class));
        return mapper.map(std, StudentDto.class);
    }

    @Override
    public StudentDto getStudent(int id) {
        return mapper.map(repo.findById(id).get(), StudentDto.class);
    }

    @Override
    public StudentDto updateStudent(int id, StudentInDto dto) {
        Student std = mapper.map(dto, Student.class);
        std.setId(id);
        return mapper.map(repo.save(std), StudentDto.class);
    }

    @Override
    public void deleteStudent(int id) {
        repo.deleteById(id);
    }
}
