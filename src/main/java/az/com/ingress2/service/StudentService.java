package az.com.ingress2.service;

import az.com.ingress2.dto.StudentDto;
import az.com.ingress2.dto.StudentInDto;

public interface StudentService {
    StudentDto createStudent(StudentInDto dto);
    StudentDto getStudent(int id);
    StudentDto updateStudent(int id, StudentInDto dto);
    void deleteStudent(int id);
}
