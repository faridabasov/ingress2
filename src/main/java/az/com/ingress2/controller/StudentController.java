package az.com.ingress2.controller;

import az.com.ingress2.dto.StudentDto;
import az.com.ingress2.dto.StudentInDto;
import az.com.ingress2.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/student")
public class StudentController {

    private final StudentService service;

    @PostMapping
    StudentDto insert(@RequestBody StudentInDto dto) {
        return service.createStudent(dto);
    }

    @GetMapping("/{id}")
    StudentDto getStudents(@PathVariable("id") int id) {
        return service.getStudent(id);
    }

    @PutMapping("/{id}")
    StudentDto getStudents(@PathVariable("id") int id, @RequestBody StudentInDto dto) {
        return service.updateStudent(id, dto);
    }

    @DeleteMapping("/{id}")
    void deleteStudents(@PathVariable("id") int id) {
        service.deleteStudent(id);
    }
}
