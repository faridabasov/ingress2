package az.com.ingress2.repository;

import az.com.ingress2.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StrudentRepository extends JpaRepository<Student, Integer> {
}
